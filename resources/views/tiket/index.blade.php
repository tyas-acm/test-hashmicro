@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tiket Saya</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Tiket Saya</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Tiket Saya
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Event</th>
                                    <th>Penyelenggara</th>
                                    <th>Tanggal & Waktu</th>
                                    <th>Lokasi</th>
                                    <th>Jenis Tiket</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tiket as $no => $t)
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td>{{ $t->order->event->nama_event ?? '' }}</td>
                                        <td>{{ $t->order->event->penyelenggara ?? '' }}</td>
                                        <td>{{ $t->order->event->tanggal ?? '' }} , {{ $t->order->event->jam ?? '' }}</td>
                                        <td>{{ $t->order->event->lokasi ?? '' }}</td>
                                        <td>{{ $t->jenis_tiket ?? '' }}</td>
                                        <td style="text-align:center">
                                            <a href="/tiket/{{$t->kode_tiket}}/detail" onclick="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                      
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>
    </section>

@endsection
