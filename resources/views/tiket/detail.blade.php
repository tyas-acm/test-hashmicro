@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Detail Tiket</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/tiket">Tiket</a></li>
                <li class="breadcrumb-item active">Detail Tiket</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header text-center">
                            Kode Tiket
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="container text-center" style="margin-top:10px;margin-bottom:10px">
                                <img class='barcode' src="https://api.qrserver.com/v1/create-qr-code/?data=HelloWorld&amp;size=100x100" alt=""  width="200" height="200" style="margin:20px" />
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix text-center">
                            Scan QrCode
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        generateBarCode();
        function generateBarCode() {
            var nric = '{{ $tiket->kode_tiket }}';
            var url = 'https://api.qrserver.com/v1/create-qr-code/?data=' + nric + '&amp;size=50x50';
            $('.barcode').attr('src', url);
        }
    </script>
@endsection