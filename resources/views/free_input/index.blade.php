@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Free Input</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Free Input</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
             
              <!-- form start -->
              <form method="post" action="/free_input/check">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="input_1">Input 1</label>
                    <input type="text" name="input_1" class="form-control" id="input_1" placeholder="Masukkan input 1">
                  </div>
                  <div class="form-group">
                    <label for="input_2">Input 2</label>
                    <input type="text" name="input_2" class="form-control" id="input_2" placeholder="Masukkan input 2">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer text-center">
                  <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>

          <!-- right column -->
          
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="card card-primary">
                
                    <div class="card-body">
                        <p><b> Input 1 : </b> {{ $input_1 ?? '' }}</p>
                        <p><b> Input 2 : </b> {{ $input_2 ?? '' }}</p>
                        <p class="text-success"><b> Hasil : {{ round($result ?? '') }}% </b></p>
                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->
            </div>
          

        </div>
      </div>
    </section>

@endsection

