@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>User</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">User</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List User</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nama User</th>
                                    <th>Email</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user as $u)
                                    <tr>
                                        <td>{{ $u->id ?? '' }}</td>
                                        <td>{{ $u->nama ?? '' }}</td>
                                        <td>{{ $u->email ?? '' }}</td>
                                        <td style="text-align:center">
                                            <a href="/user/{{$u->id}}/delete" onclick="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash text-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                      
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>
    </section>

@endsection
