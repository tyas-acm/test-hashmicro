@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Default box -->
    @if ( Session::get('name') == 'Admin' )
    <div class="card">
        
        <div class="card-body">
            <b>Welcome {{Session::get('name')}} !</b>
        </div>
        
    </div>
    @else
    <div class="container">
        <div class="row">
        @foreach($event as $e)
            <div class="col-sm-4">
                <!-- <div class="card-columns-fluid"> -->
                    <div class="card" style="width: 20rem;">
                        <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
                        <div class="card-body">
                            <h5 class="card-title"><b>{{ $e->nama_event ?? '' }}</b></h5>
                            <p class="card-text">( {{ $e->penyelenggara ?? '' }} )</p>
                            <h6 class="card-text">Lokasi : {{ $e->lokasi ?? '' }}</h6>
                            <h6 class="text-secondary">{{ $e->tanggal ?? '' }}</h6>
                            <h6 class="text-secondary">( {{ $e->jam ?? '' }} )</h6>
                            <h5 class="card-text text-success">Rp. {{ $e->harga ?? '' }}</h5>
                            <a href="/order/{{ $e->kode_event }}/add" class="btn btn-success">Beli Tiket</a>
                        </div>
                    </div>
                <!-- </div> -->
            </div>
        @endforeach
        </div>
    </div>
    @endif

@endsection
