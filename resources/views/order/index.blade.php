@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Riwayat Order</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Riwayat Order</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Riwayat Order
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    @if( Session::get('name') == 'Admin' )
                                    <th>Pemesan</th>
                                    @endif
                                    <th>Nama Event</th>
                                    <th>Jumlah Beli</th>
                                    <th>Harga</th>
                                    <th>Diskon</th>
                                    <th>Total</th>
                                    <!-- <th style="text-align:center">Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order as $no => $o)
                                
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        @if( Session::get('name') == 'Admin' )
                                        <td>{{  $o->user->nama ?? '' }}</td>
                                        @endif
                                        <td>{{ $o->event->nama_event ?? '' }}</td>
                                        <td>{{ $o->jumlah ?? '' }}</td>
                                        <td>{{ $o->event->harga ?? '' }}</td>
                                        <td>{{ $o->diskon ?? '' }}</td>
                                        <td>{{ $o->total ?? '' }}</td>
                                        <!-- <td style="text-align:center">
                                            <a href="/order/{{$o->kode_order}}/detail" onclick="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye" aria-hidden="true"></i></a>
                                        </td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                      
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>
    </section>

@endsection
