@extends('template')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Beli Tiket</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/order">Order</a></li>
                <li class="breadcrumb-item active">Beli Tiket</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
                <div class="card-header">
                    Event : {{ $event->nama_event ?? '' }}
                </div>
              <!-- form start -->
              <form method="post" action="/order/add_action">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jenis">Jenis Tiket</label>
                                <select class="form-control" name="jenis_tiket" id="jenis_tiket">
                                    <option hidden selected disable>Pilih Jenis Tiket</option>
                                    <option value="biasa">Biasa</option>
                                    <option value="vip">VIP</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jumlah">Jumlah Tiket</label>
                                <input type="number" name="jumlah" class="form-control" id="jumlah" value="0" placeholder="Jumlah Tiket">
                                <input type="hidden" class="form-control" id="harga" value={{ $event->harga ?? '' }}>
                                <input type="hidden" name="kode_event" class="form-control" value={{ $event->kode_event ?? '' }}>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer text-right">
                    <div><input style="border: none; outline: none;" name="total" id="total"></div>
                    <button type="submit" id="submit" class="btn btn-primary">Order</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          

        </div>
      </div>
    </section>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $('#submit').unbind('click').bind('click', function confirmClose(e){
        if(!confirm('Anda yakin data akan disimpan? '))
            e.preventDefault();
    });

    $( document ).ready(function(){
        $('#jumlah').change(function(){
            var jumlah = $('#jumlah').val();
            var harga = $('#harga').val();
            var total = jumlah*harga;

            $('#total').val(total);
        });
    });
</script>
@endsection
