@extends('template')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Event</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/event">Event</a></li>
                <li class="breadcrumb-item active">Edit Event</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
             
              <!-- form start -->
              <form method="post" action="/event/edit_action">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Nama Event</label>
                                <input type="text" name="nama" class="form-control" id="nama" value="{{ $event->nama_event ?? '' }}" placeholder="Nama Event">
                                <input type="hidden" name="kode_event" class="form-control" value="{{ $event->kode_event ?? '' }}">
                            </div>
                            <div class="form-group">
                                <label for="penyelenggara">Penyelenggara</label>
                                <input type="text" name="penyelenggara" class="form-control" id="penyelenggara" value="{{ $event->penyelenggara ?? '' }}" placeholder="Penyelenggara">
                            </div>
                            <div class="form-group">
                                <label for="time">Tanggal & Waktu</label>
                                <input type="text" name="time" class="form-control float-right" id="reservationtime" placeholder="Tanggal & Waktu">
                                <!-- <input type="text" name="penyelenggara" class="form-control" id="penyelenggara" placeholder="Penyelenggara"> -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lokasi">Lokasi</label>
                                <input type="text" name="lokasi" class="form-control" id="lokasi" value="{{ $event->lokasi ?? '' }}" placeholder="Lokasi">
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input type="text" name="harga" class="form-control" id="harga" value="{{ $event->harga ?? '' }}" placeholder="Harga">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option hidden selected disable>Pilih Status</option>
                                    <option value="0" {{ $event->status == 0 ? 'selected' : '' }}>Tidak Aktif</option>
                                    <option value="1" {{ $event->status == 1 ? 'selected' : '' }}>Aktif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer text-right">
                    <a href="/event" class="btn btn-danger">Batal</a>
                    <button type="submit" id="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          

        </div>
      </div>
    </section>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $('#submit').unbind('click').bind('click', function confirmClose(e){
        if(!confirm('Anda yakin data akan diupdate ? '))
            e.preventDefault();
    });

    $( document ).ready(function(){
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'DD/MM/YYYY hh:mm A',
                cancelLabel: 'Clear'
            }
        });
    });
</script>
@endsection
