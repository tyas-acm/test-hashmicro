@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>List Event</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Event</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="/event/add" class="btn btn-primary btn-block">Buat Event</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Event</th>
                                    <th>Penyelenggara</th>
                                    <th>Tanggal & Waktu</th>
                                    <th>Lokasi</th>
                                    <th>Harga</th>
                                    <th>Status</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($event as $no => $e)
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td>{{ $e->nama_event ?? '' }}</td>
                                        <td>{{ $e->penyelenggara ?? '' }}</td>
                                        <td>{{ $e->tanggal ?? '' }} , {{ $e->jam ?? '' }}</td>
                                        <td>{{ $e->lokasi ?? '' }}</td>
                                        <td>{{ $e->harga ?? '' }}</td>
                                        <td>
                                            @if($e->status == 0)
                                                <span class="badge bg-warning">Tidak Aktif</span>
                                            @else
                                                <span class="badge bg-success">Aktif</span>
                                            @endif
                                        </td>
                                        <td style="text-align:center">
                                            <a href="/event/{{$e->kode_event}}/edit" onclick="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt text-success" aria-hidden="true"></i></a>
                                            <a href="/event/{{$e->kode_event}}/delete" onclick="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash text-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                      
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>
    </section>

@endsection
