@extends('template')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah Event</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/event">Event</a></li>
                <li class="breadcrumb-item active">Tambah Event</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
             
              <!-- form start -->
              <form method="post" action="/event/add_action">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Nama Event</label>
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Event">
                            </div>
                            <div class="form-group">
                                <label for="penyelenggara">Penyelenggara</label>
                                <input type="text" name="penyelenggara" class="form-control" id="penyelenggara" placeholder="Penyelenggara">
                            </div>
                            <div class="form-group">
                                <label for="time">Tanggal & Waktu</label>
                                <input type="text" name="time" class="form-control float-right" id="reservationtime" placeholder="Tanggal & Waktu">
                                <!-- <input type="text" name="penyelenggara" class="form-control" id="penyelenggara" placeholder="Penyelenggara"> -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lokasi">Lokasi</label>
                                <input type="text" name="lokasi" class="form-control" id="lokasi" placeholder="Lokasi">
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input type="text" name="harga" class="form-control" id="harga" placeholder="Harga">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer text-right">
                    <button type="submit" id="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          

        </div>
      </div>
    </section>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $('#submit').unbind('click').bind('click', function confirmClose(e){
        if(!confirm('Anda yakin data akan disimpan? '))
            e.preventDefault();
    });

    $( document ).ready(function(){
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'DD/MM/YYYY hh:mm A'
            }
        });
    });
</script>
@endsection
