<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    protected $table = 'tiket';
    protected $primaryKey = 'kode_tiket';
    public $incrementing = false;
    protected $fillable = [
        'kode_tiket',
        'id',
        'kode_order', 
        'jenis_tiket',
        'status'
    ];

    public function order(){
		return $this->belongsTo('App\Order', 'kode_order');
	}

}
