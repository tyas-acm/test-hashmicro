<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $primaryKey = 'kode_order';
    public $incrementing = false;
    protected $fillable = [
        'kode_order',
        'id',
        'kode_event',
        'jumlah',
        'total',
        'diskon',
        'total_bayar',
        'status'
    ];

    public function event(){
		return $this->belongsTo('App\Event', 'kode_event');
	}

    public function user(){
		return $this->belongsTo('App\User', 'id');
	}
}
