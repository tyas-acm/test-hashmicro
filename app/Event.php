<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'event';
    protected $primaryKey = 'kode_event';
    public $incrementing = false;
    protected $fillable = [
        'kode_event',
        'nama_event',
        'penyelenggara',
        'tanggal',
        'jam',
        'lokasi', 
        'harga', 
        'status'
    ];
}
