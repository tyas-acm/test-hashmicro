<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{

    //** EVENT PAGE */
    public function index(){
        $event = Event::get();

        return view('/event/index', [
            'event' => $event
        ]);
    }

    //** ADD EVENT */
    public function add(){
        return view('/event/add');
    }

    //** ADD ACTION */
    public function add_action(Request $request){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        date_default_timezone_set("Asia/Jakarta");
        $kode_event = 'EV'.date("ymdhis".$micro, $t);

        $time = explode(' - ', $request->time);
        $time_1 = explode(' ', $time[0]);
        $time_2 = explode(' ', $time[1]);

        Event::create([
            'kode_event' => $kode_event,
            'nama_event' => $request->nama,
            'penyelenggara' => $request->penyelenggara,
            'tanggal' => $time_1[0].' to '.$time_2[0],
            'jam' => $time_1[1].$time_1[2].' - '.$time_2[1].$time_2[2],
            'lokasi' => $request->lokasi,
            'harga' => $request->harga,
            'status' => 1
        ]);

        return redirect('/event')->with('success', 'Event berhasil ditambahkan');
    }

    //** EDIT EVENT */
    public function edit($id){
        $event = Event::where('kode_event', $id)->first();

        return view('/event/edit', [
            'event' => $event
        ]);
    }

    //** EDIT ACTION */
    public function edit_action(Request $request){
        $time = explode(' - ', $request->time);
        $time_1 = explode(' ', $time[0]);
        $time_2 = explode(' ', $time[1]);

        Event::where('kode_event', $request->kode_event)->update([
            'nama_event' => $request->nama,
            'penyelenggara' => $request->penyelenggara,
            'tanggal' => $time_1[0].' to '.$time_2[0],
            'jam' => $time_1[1].$time_1[2].' - '.$time_2[1].$time_2[2],
            'lokasi' => $request->lokasi,
            'harga' => $request->harga,
            'status' => $request->status
        ]);

        return redirect('/event')->with('success', 'Data Event Berhasil diubah');
    }

    //** DELETE EVENT */
    public function delete($id){
        $event = Event::where('kode_event', $id);
        $event->delete();

        return redirect('/event')->with('success', 'Data Event berhasil dihapus');
    }

}
