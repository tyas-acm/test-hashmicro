<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    //** USER PAGE */
    public function index(){
        $user = User::get();

        return view('/user/index', [
            'user' => $user
        ]);
    }

    //** DELETE ACTION */
    public function delete($id){
        $user = User::find($id);
        $user->delete();

        return redirect('/user')->with('success', 'Data User berhasil dihapus');
    }

}
