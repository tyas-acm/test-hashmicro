<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class DashboardController extends Controller
{

    //** DASBOARD PAGE */
    public function index(){
        $event = Event::where('status', 1)->get();

        return view('welcome', [
            'event' => $event
        ]);
    }
}
