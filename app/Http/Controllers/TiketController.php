<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Tiket;

class TiketController extends Controller
{

    //** TIKET PAGE */
    public function index(){
        $tiket = Tiket::where('id', Session::get('id'))->get();

        return view('/tiket/index', [
            'tiket' => $tiket
        ]);
    }

    //** DETAIL PAGE */
    public function detail($id){
        $tiket = Tiket::where('kode_tiket', $id)->first();

        return view('/tiket/detail', [
            'tiket' => $tiket
        ]);
    }

}
