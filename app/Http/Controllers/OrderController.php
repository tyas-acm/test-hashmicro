<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Order;
use App\Tiket;
use Session;
use Str;

class OrderController extends Controller
{

    //** ORDER PAGE */
    public function index(){
        $order = Order::where('id', Session::get('id'))->get();

        return view('/order/index', [
            'order' => $order
        ]);
    }

    //** ADD ORDER */
    public function add($id){
        $event = Event::where('kode_event', $id)->first();

        return view('/order/add', [
            'event' => $event
        ]);
    }

    //** ADD ACTION */
    public function add_action(Request $request){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        date_default_timezone_set("Asia/Jakarta");
        $kode_order = 'OR'.date("ymdhis".$micro, $t);

        $event = Event::where('kode_event', $request->kode_event)->first();

        // DISKON JIKA MEMBELI LEBIH DARI 3 TIKET
        // NESTED IF
        if ( $event->harga > 0 ){
            if ( $request->jumlah >= 3 ){
                if ( $request->jenis_tiket == 'biasa' ){
                    $diskon = $request->total*5/100;
                    $total_bayar = $request->total - $diskon;
                } else if ( $request->jenis_tiket == 'vip'){
                    $diskon = $request->total*20/100;
                    $total_bayar = $request->total - $diskon;
                } else {
                    $diskon = 0;
                    $total_bayar = $request->total - $diskon;
                }
            } else {
                $diskon = 0;
                $total_bayar = $request->total - $diskon;
            }
        } else {
            $diskon = 0;
            $total_bayar = 0;
        }

        // ORDER CREATE
        Order::create([
            'kode_order' => $kode_order,
            'id' => Session::get('id'),
            'kode_event' => $event->kode_event,
            'jumlah' => $request->jumlah,
            'total' => $request->total,
            'diskon' => $diskon,
            'total_bayar' => $total_bayar,
            'status' => 1
        ]);

        // TIKET CREATE
        for( $i = 1; $i <= $request->jumlah; $i++ ){
            Tiket::create([
                'kode_tiket' => Str::random(30),
                'id' => Session::get('id'),
                'kode_order' => $kode_order, 
                'kode_event' => $event->kode_event,
                'jenis_tiket' => $request->jenis_tiket,
                'status' => 1
            ]);
        }

        return redirect('/dashboard')->with('success', 'Tiket event berhasil dibeli');
    }

}
