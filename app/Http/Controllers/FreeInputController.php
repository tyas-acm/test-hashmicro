<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FreeInputController extends Controller
{

	//** INDEX FREE INPUT */
    public function index(){
        return view('/free_input/index');
    }

	//** CHECK FREE INPUT */
    public function check(Request $request) {

		$input1 = $request->input_1;
		$input1 = array_unique(str_split(strtolower($input1)));
		$input1 = array_values($input1);
		$input2 = $request->input_2;
		$input2 = array_unique(str_split(strtolower($input2)));
		$input2 = array_values($input2);
		$total = 0;

        // Nested Loop
		foreach($input1 as $in1=>$x){
			foreach($input2 as $in2=>$y){
				if($input1[$in1] == $input2[$in2]){
					$total++;
				}
			}
		}

		$result = $total/strlen($request->input_1)*100;
		
		return view('/free_input/index',[
			'result' => $result,
			'input_1' => $request->input_1,
			'input_2' => $request->input_2
		]);
	}

}
