<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->string('kode_event',50);
            $table->primary('kode_event');
            $table->string('nama_event',50);
            $table->string('penyelenggara',50);
            $table->string('tanggal',50);
            $table->string('jam', 50);
            $table->string('lokasi', 50);
            $table->string('harga', 50);
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
