<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', 'DashboardController@index');

Route::get  ('/', 'LoginController@index');
Route::get  ('/logout', 'LoginController@logout');
Route::post ('/authenticating', 'LoginController@authenticating');

Route::get  ('/register', 'LoginController@register');
Route::post ('/register/action', 'LoginController@register_action');

Route::get  ('/free_input', 'FreeInputController@index');
Route::post ('/free_input/check', 'FreeInputController@check');

Route::get  ('/math_table', 'MathTableController@index');
Route::post ('/math_table/check', 'MathTableController@check');

Route::get  ('/table', 'FreeInputController@table');

Route::get  ('/user', 'UserController@index');
Route::get  ('/user/{id}/delete', 'UserController@delete');

Route::get  ('/profile', 'ProfileController@index');
Route::get  ('/profile/{id}/edit', 'ProfileController@edit');
Route::post ('/profile/edit_action', 'ProfileController@edit_action');

// EVENT
Route::get  ('/event', 'EventController@index');
Route::get  ('/event/add', 'EventController@add');
Route::post ('/event/add_action', 'EventController@add_action');
Route::get  ('/event/{id}/edit', 'EventController@edit');
Route::post ('/event/edit_action', 'EventController@edit_action');
Route::get  ('/event/{id}/delete', 'EventController@delete');

// ORDER
Route::get  ('/order', 'OrderController@index');
Route::get  ('/order/{id}/add', 'OrderController@add');
Route::post ('/order/add_action', 'OrderController@add_action');

// TIKET
Route::get  ('/tiket', 'TiketController@index');
Route::get  ('/tiket/{id}/detail', 'TiketController@detail');
